const mongoose = require('mongoose');
const config = require('../config/database');
mongoose.Promise = global.Promise;
const Schema = mongoose.Schema;

//Validate title
let titleLengthChecker = (title) => {
	if (!title){
		return false;
	} else {
		if (title.length < 5 || title.title > 50){
			return false;
		} else {
			return true;
		}
	}
};

let alphaNumericTitleChecker = (title) => {
	if (!title){
		return false;
	} else {
		const regExp = new RegExp(/^[a-zA-Z0-9 ]+$/);
		return regExp.test(title);
	}
};

const titleValidators = [
	{validator: titleLengthChecker, 
	message: 'Title must be more than 5 characters but not more than 50'},

	{validator: alphaNumericTitleChecker, message: 'Title must be alphanumeric'}
];

// Validate the body ofmessage
let bodyLengthChecker = (body) => {
	if (!body){
		return false;
	} else {
		if (body.length < 5 || body.body > 140){
			return false;
		} else {
			return true;
		}
	}
};

const bodyValidators = [
	{
		validator: bodyLengthChecker,
		message: 'Body must be more than 5 characters but not more than 140 '
	}

];

// Validate comment length
let commentLengthChecker = (comment) => {
	if (!comment[0]){
		return false;
	} else{
		if (comment[0].length < 1 || comment[0].length > 140){
			return false;
		} else {
				return true;
			}	
	}
};

const commentValidators = [
	{
		validator: commentLengthChecker,
		message: 'Comment may not exceed 140 characters'
	}
];

//Blog Schema
const blogSchema = new Schema({
	title: {type: String, required: true, validate: titleValidators},
	body: {type: String, required: true, validate: bodyValidators},
	createdBy: {type: String},
	createdAt: {type: Date, default: Date.now()},
	likes: {type: Number, default: 0},
	likedBy: [{ type: Schema.Types.ObjectId, ref: 'User' }],
	dislikes: {type: Number, default: 0},
	dislikedBy: [{ type: Schema.Types.ObjectId, ref: 'User' }],
	comments: [{
		comment: { type: String, validate: commentValidators },
		commentator: { type: String }	 
	}]

}); 

module.exports = mongoose.model('Blog', blogSchema);