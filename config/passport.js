const JwtStrategy = require('passport-jwt').Strategy;
const ExtractJwt = require('passport-jwt').ExtractJwt;
const User = require('../models/user');
const config = require('../config/database');


module.exports = function(passport){
	let opts = {};
	opts.jwtFromRequest = ExtractJwt.fromAuthHeaderWithScheme('jwt');
	opts.secretOrKey = config.secret;
	passport.use(new JwtStrategy(opts, function(jwt_payload, done){
		//User.getUserByMail(jwt_payload.data.email, function(err, user){
		User.getUserById(jwt_payload.data._id, function(err, user){
			console.log(jwt_payload);
			if(err){
				console.log("ERROR");
				return done(err, false);
			}
			if(user){
				//console.log("USER EXIST");
				return done(null, user);
			} else {
								//console.log("USER NOT EXXIST");
				 return done(null, false);
			}
		});
	}));
}