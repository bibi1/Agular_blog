
const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const token = require('./users');
const User = require('../models/user');
const Blog = require('../models/blog');


module.exports = (router) => {
	router.post('/newBlog', (req, res) => {
		if(!req.body.title){
			res.json({ success: false, message: 'Blog title is required' });	
		} else {
			if (!req.body.body){
				res.json({ success: false, message: 'Blog body is required' });
			} else {
				if (!req.body.createdBy){
					res.json({ success: false, message: 'Blog createdBy is required' });
				} else{
					const blog = new Blog({
						title: req.body.title,
						body: req.body.body,
						createdBy: req.body.createdBy
					});

					blog.save((err) => {
						if(err) {
							if (err.errors){
								if (err.errors.title){
									res.json({ success: false, 
										message: err.errors.title.message 
									});

								} else {
									if (err.errors.body){
										res.json({ success: false,
										 message: err.errors.body.message 
										});

									} else{
										res.json({ success: false, 
											message: err.errmsg
										});
									}
								}
							}	else {
									res.json({
										success: false, 
										message: err 
								   });
								}

						} else {
								res.json({
									success: true, 
									message: 'Blog saved!'
								});
							}
					});
			  	}
			}
		}
	});

router.get('/allBlogs', function(req, res){
	Blog.find({}).populate('likedBy dislikedBy').exec(function(err, blogs){
		if(err){
		res.json({ success: false, message: err });
		} else {
			if (!blogs){
				res.json({ success: false, message: 'No blogs found'});
			} else {
				res.json({ success: true, blogs: blogs });
			}
		}
	})//.sort({ '_id': -1 });
});

	router.get('/singleBlog/:id', function(req, res){
		if(!req.params.id){
			res.json({ success: false, message: 'No blog ID was provided' });
		} else {
			Blog.findOne({ _id:req.params.id }, function(err, blog){
				if (err){
					res.json({ success: false, message: 'No a valid blog id' });
				} else {
					if (!blog){
						res.json({ success: false, message: 'Blog found'});
					} else {
						res.json({ success: true, blog: blog });
					}
				}
			});
		}
			
	});

	router.put('/updateBlog/:id', function(req, res){
		if(!req.params.id){
			res.json({ success: false, message: 'No blog id provided' });
		} else{
			console.log(req.params.id);
			Blog.findOne({_id: req.params.id }, function(err, blog){
				if (err){
					res.json({ success: false, message: 'No a valid blog id' });
				} else {
					if (!blog){
						res.json({ success: false, message: 'Blog id not found'});
					} else {
						blog.title = req.body.title;
						blog.body = req.body.body;
						blog.createdBy = req.body.createdBy;
						 // blog.save();
						 blog.save((err) => {
						if(err) {
							if (err.errors){
								if (err.errors.title){
									res.json({ success: false, 
										message: err.errors.title.message 
									});

								} else {
									if (err.errors.body){
										res.json({ success: false,
										 message: err.errors.body.message 
										});

									} else{
										res.json({ success: false, 
											message: err.errmsg
										});
									}
								}
							}	else {
									res.json({
										success: false, 
										message: err 
								   });
								}

						} else {
								res.json({
									success: true, 
									message: 'Blog saved!'
								});
							}
					});
				
					}
				}	
			}); 
		}
	});


router.delete('/deleteBlog/:id',passport.authenticate('jwt', {session:false}), function(req, res){
		if(!req.params.id){
			res.json({ success: false, message: 'No id provided' });
		} else{
			console.log(req.params.id);
			Blog.findOne({_id: req.params.id }, function(err, blog){
				if (err){
					res.json({ success: false, message: 'Invalid Id' });
				} else {
					if (!blog){
						res.json({ success: false, message: 'ID was not found'});
					} else {
						if(req.user._id == blog.createdBy){
							blog.remove(function(err){
								if(err){
									res.json({ success: false, message: err });
								} else {
									res.json({ success: true, message: 'Blog deleted' });
								}
								
							});	
						} else {
							res.json({success: false, message: 'You are not authorize to delete this blog post'});
						}		
					}			
				}
				
	    	});
	    }	
});    


	router.put('/likeBlog', passport.authenticate('jwt', {session:false}), function(req, res) {
		if(!req.body.id){
			res.json({success: false, message: 'No id provided'});
		} else {
			Blog.findOne({ _id: req.body.id}, function(err, blog){
				if (err){
						res.json({ success: false, message: 'Invalid blod id'});
					} else {
						if(!blog){
							res.json({ success: false, message: 'That blog was found' });
						} else {
							if(req.user._id == blog.createdBy){
								res.json({success: false, message: 'You can not like your own blog'});
							} else {
								if(blog.likedBy.includes(req.user._id)){
									res.json({success: false, message: 'You already liked this blog'});
								} else {
									if (blog.dislikedBy.includes(req.user._id)){
										blog.dislikes--;
										const arrayIndex = blog.dislikedBy.indexOf(req.user._id);
										blog.dislikedBy.splice(arrayIndex, 1);
										blog.likes++;
										blog.likedBy.push(req.user._id);
											blog.save(function(err){
												if(err){
													res.json({success:false, message: 'Something went wrong'});
												} else {
													res.json({success: true, message: 'Blog liked'});
												}
											});
									} else {
										blog.likes++;
										blog.likedBy.push(req.user._id);
										blog.save(function(err){
											if(err){
												res.json({success:false, message: 'Something went wrong'});
											} else {
												res.json({success: true, message: 'Blog liked'});
											}
										});
									}
								}
							}
						}				
					}
				});			
			}
	});	

	
	router.put('/dislikeBlog', passport.authenticate('jwt', {session:false}), function(req, res) {
		if(!req.body.id){
			res.json({success: false, message: 'No id provided'});
		} else {
			Blog.findOne({ _id: req.body.id}, function(err, blog){
				if (err){
						res.json({ success: false, message: 'Invalid blod id'});
					} else {
						if(!blog){
							res.json({ success: false, message: 'That blog was found' });
						} 
							if(req.user._id == blog.createdBy){
								res.json({success: false, message: 'You can not dislike your own blog'});
							} else {
								if(blog.likedBy.includes(req.user._id)){
									res.json({success: false, message: 'You already disliked this blog'});
								} else {
									if (blog.likedBy.includes(req.user._id)){
										blog.likes--;
										const arrayIndex = blog.likedBy.indexOf(req.user._id);
										blog.likedBy.splice(arrayIndex, 1);
										blog.dislikes++;
										blog.dislikedBy.push(user);
											blog.save(function(err){
												if(err){
													res.json({success:false, message: 'Something went wrong'});
												} else {
													res.json({success: true, message: 'Blog disliked'});
												}
											});
									} else {
										blog.dislikes++;
										blog.dislikedBy.push(req.user._id);
										blog.save(function(err){
											if(err){
												res.json({success:false, message: 'Something went wrong'});
											} else {
												res.json({success: true, message: 'Blog disliked'});
											}
										});
									}
								}
							}				
						}
				});			
			}
	});	

	router.post('/comment', function(req, res){
		if(!req.body.comment){
			res.json({ success: false, message: 'No comment provided'});
		} else {
			if (!req.body.id){
				res.json({ success: false, message: 'No id was provided'});
			} else {
				Blog.findOne({ _id: req.body.id }, function(err, blog){
					if(err){
						res.json({success: false, message: 'Invalid id'});
					} else{
						if (!blog){
							res.json({success: false, message: 'Blog not found'});
						} else {
							User.findOne({ user: req._id }, function(err, user){
								if (err){
									res.json({success: false, message: 'Something went wrong'});
								} else{
									if(!user){
										res.json({success: false, message: 'User not found'});
									} else {
										blog.comments.push({
											comment: req.body.comment,
											commentator: user._id //user.username
										});
										blog.save(function(err){
											if (err){
												res.json({success: false, message: 'Something went wrong'});
											} else {
												res.json({success: true, message: 'Comment saved'});
											}
										});
									}
								}
							});
						}
					}
				});
			}
		}
	});

	return router;
};



