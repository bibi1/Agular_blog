const express = require('express');
const router = express.Router();
const passport = require('passport');
const jwt = require('jsonwebtoken');
const config = require('../config/database');

const multer = require('multer');
//const DIR = './uploads/';
//const upload = multer({dest: DIR}).single('photo');


const User = require('../models/user');

//Register
router.post('/register', function(req, res, next){
	let newUser = new User({
		name: req.body.name,
		email: req.body.email,
		username: req.body.username,
		password: req.body.password
	});
	User.addUser(newUser, function(err, user){
		if(err){
			res.json({success: false, msg: 'Failed to register user'});	
		} else {
			res.json({success: true, msg: 'User registerd'});	
		}
	});
	
});	

//Authentication 
router.post('/authenticate', function(req, res, next){
	const username = req.body.username;
	const password = req.body.password;
	User.getUserByUsername(username, function(err, user){
		if(err) throw err;
		if(!user){
			return res.json({success: false, msg: 'User not found'});
		}
		User.comparePassword(password, user.password, function(err, isMatch){
			if(err) throw err;
			if(isMatch){
				const token = jwt.sign({data: user}, config.secret, {
					expiresIn: 604800
				});
				res.json({
					success: true,
					token: 'JWT ' 
					+token,
					user: {
						id: user._id,
						name: user.name,
						username: user.username,
						email: user.email

					}
				});
			} else {
				return res.json({success: false, msg: 'Wrong password'});
			}
		});
	});
});	

router.put('/updateUser', function(req, res, next) {
  var user = new User(req.body);

  User.update({_id : user.id}, user, function(err) {
    if (err) {
      console.log("not updated!");
      res.status(400);      
      res.send();
    }
    console.log("user updated!");
    res.send({status: 'ok'});
  });
});

router.delete('/deleteUser/:_id', function(req, res, next) {
  User.remove({_id : req.params._id}, function(err) {
    if (err) {
      res.json({success: false, message: 'user not removed'});
    }
   res.json({success: true, message: 'user removed'});
  });
});

//Profile
router.get('/profile', passport.authenticate('jwt', {session:false}), function(req, res, next){
	res.json({user: req.user});
});

router.get('/publicProfile/:_id', function(req, res){
	if(!req.params._id){
		res.json({ success: false, message: 'No id was provided'});
	} else {
		User.findOne({_id: req.params._id}).select('_id username email').exec(function(err, user){
			if(err){
				res.json({success:false, message: 'something went wrong'});
			} else {
				if(!user){
					res.json({ success: false, message: 'Id not found'});
				} else {
					res.json({success: true, user: user});
				}
			}
		});
	}
});

router.get('/getAllUsers', function(req, res){
	User.find({}, function(err, users){
		if (err){
			res.json({ success: false, message: err });
		}else {
			if (!users){
				res.json({ success: false, message: 'No users found'});
			} else {
				res.json({ success: true, users: users });
			}
		}
	});
});	

module.exports = router;
