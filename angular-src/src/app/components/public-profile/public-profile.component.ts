import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import {BlogService} from '../../services/blog.service';


@Component({
  selector: 'app-public-profile',
  templateUrl: './public-profile.component.html',
  styleUrls: ['./public-profile.component.css']
})
export class PublicProfileComponent implements OnInit {
	curentUrl;
	_id;
  username;
	email;
	foundProfile = false;
	message;
	messageClass;

  constructor(
  	private authService: AuthService,
  	private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
  	this.curentUrl = this.activatedRoute.snapshot.params;
  	this.authService.getPublicProfile(this.curentUrl._id).subscribe(data => {
  		if (!data.success){
  			this.messageClass = 'alert aler-danger';
  			this.message = data.message;
  		} else {
  			this._id = data.user._id;
  			this.username = data.user.username;
  			this.email = data.user.email;
  			this.foundProfile = true;
  		}
  		
  	});
  }

  goBack(){
    //window.location.reload();
    history.back();
  }

}
