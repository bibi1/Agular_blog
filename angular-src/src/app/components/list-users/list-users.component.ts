import { Component, OnInit } from '@angular/core';
import { AuthService } from '../../services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import {BlogService} from '../../services/blog.service';

@Component({
  selector: 'app-list-users',
  templateUrl: './list-users.component.html',
  styleUrls: ['./list-users.component.css']
})
export class ListUsersComponent implements OnInit {
  usersList;
  _id;
  user;

  constructor(
  	private authService: AuthService,
  	private activatedRoute: ActivatedRoute
  ) { }

  getAllUsers(){
    this.authService.getAllUsers().subscribe(data =>{
    	console.log(data);
      this.usersList = data.users;
    });
  }

   ngOnInit() {
  	this.authService.getProfile().subscribe(profile => {
  		this._id = profile.user._id;
  	});
    this.getAllUsers();
  }

}
