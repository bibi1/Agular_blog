import { Component, OnInit,  ElementRef, Input  } from '@angular/core';
import {AuthService}  from '../../services/auth.service';
import {FormControl, FormGroup, FormBuilder, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from '../../user';
import {FlashMessagesService} from 'angular2-flash-messages';

/*import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

const URL = 'http://localhost:8000/api/upload';*/


@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.css']
})
export class ProfileComponent implements OnInit {
	user: User;
  editUser: User;
  newUser;
  form;
  processing = false;
  updateInfo = false;
  public name: String;
  public username: String;  
  public email: String;
  public password: String;

  /*public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
   profile = 'Profile Photo';*/
    

  //selectedFiles: FileList;
  //currentUpload: Upload;

  constructor(private authService: AuthService, 
  	private router: Router,
    private formBuilder: FormBuilder,
    private flashMessage: FlashMessagesService,
    //private http: Http, private el: ElementRef
    //private upSvc: UploadService
  	) { }

  ngOnInit() {
  	this.authService.getProfile().subscribe(profile => {
      console.log(profile);
  		this.user = profile.user;
  	},
  	err => {
  		console.log(err);
  		return false;
  	});
    this.editUser = User.CreateDefault();
  }

  creatNewUpdateForm(){
    this.form = this.formBuilder.group({
      name: {
        type: String,
        required: true
      },
      email: {
        type: String,
        required: true
      },
      username: {
        type: String,
        required: true
      },
      password: {
        type: String,
        required: true
      }
    });
      this.editUser.username = this.user.username;
      this.editUser.name = this.user.name;
      this.editUser.email = this.user.email;
      this.editUser.password = this.user.password;   
  }

  enableFormNewUpdateForm(){
    this.form.get('name').enable();
    this.form.get('email').enable();
    this.form.get('username').enable();
    this.form.get('password').enable();
  }

  disableFormNewUpdateForm(){
    this.form.get('name').disable();
    this.form.get('email').disable();
    this.form.get('username').disable();
    this.form.get('password').disable();
  }

  newUpdateForm(){
    this.creatNewUpdateForm();
    this.updateInfo = true;  
  }

  public onUpdateSubmit(){
     const user = {
      name: this.name,
      email: this.email,
      username: this.username,
      password: this.password    
    }
    this.authService.updateUser(user).subscribe(data => {
      const index = this.user;
      this.user = this.editUser;
      this.editUser = User.CreateDefault();
        if(data.success){
          this.flashMessage.show('User updated ', {cssClass: 'alert-success', timeout: 3000});
          this.router.navigate(['profile']);
        } else {
          this.flashMessage.show('Something went wrong ', {cssClass: 'alert-danger', timeout: 3000});
          //this.router.navigate(['/updateUser']);
          this.updateInfo = false;
        }
      });
  }  

  deleteUser(user) {
  this.authService.deleteUser(user).subscribe(data => {
      this.user;
      this.flashMessage.show('User deleted!', {cssClass: 'alert-danger', timeout: 3000});
      this.router.navigate(['/register'])
    })
  }

   goBack(){
    this.updateInfo = false;
    //window.location.reload();
  }

}
