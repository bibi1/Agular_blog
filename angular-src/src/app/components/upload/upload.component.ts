import { Component, OnInit, ElementRef, Input, ViewChild	} from '@angular/core';
import {AuthService} from '../../services/auth.service';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {FlashMessagesService} from 'angular2-flash-messages';
import {  FileUploader } from 'ng2-file-upload/ng2-file-upload';
import { Http, Response } from '@angular/http';
import "rxjs/add/operator/do";
import "rxjs/add/operator/map";

const URL = 'http://localhost:3000/upload';

@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})

export class UploadComponent implements OnInit{
	form: FormGroup;
  	loading: boolean = false;
    public uploader:FileUploader = new FileUploader({url: URL, itemAlias: 'photo'});
    title = 'Profile Photo';
    img: { data: Buffer, contentType: String };
 
 @ViewChild('fileInput') fileInput: ElementRef;

constructor(private http: Http, 
	private el: ElementRef,
	private authService: AuthService,
    private flashMessage: FlashMessagesService,
    private fb: FormBuilder
    ){ 
		
	}

	ngOnInit() {
	     this.createForm();
	 }
   	

	createForm() {
    	this.form = this.fb.group({
	      avatar: null
	    });
	}

	onFileChange(event) {
    let reader = new FileReader();
    if(event.target.files && event.target.files.length > 0) {
      let file = event.target.files[0];
      reader.readAsDataURL(file);
      reader.onload = () => {
        this.form.get('avatar').setValue({
          filename: file.name,
          filetype: file.type,
          value: reader.result.split(',')[1]
        })
      };
    }
  }

  clearFile() {
    this.form.get('avatar').setValue(null);
    this.fileInput.nativeElement.value = '';
  }

  onSubmit() {
    const formModel = this.form.get('avatar').value;
    console.log(formModel);
    this.loading = true;
    var formData = new FormData();
    formData.append('myfile',formModel, formModel.filename);
    // In a real-world app you'd have a http request / service call here like
     this.authService.uploadPhoto(formData).subscribe(data => {
      if(data.success){
        this.flashMessage.show('You have uploaded the a Profile photo!', {
          cssClass: 'alert-success', 
          timeout: 5000});
      } else {
        this.flashMessage.show(data.message, {
          cssClass: 'alert-danger', 
          timeout: 5000});
      }
    });
  }
}



 /*
	ngOnInit() {
	    this.uploader.onAfterAddingFile = (myfile)=> { myfile.withCredentials = false; };
	    this.uploader.onCompleteItem = (item:any, response:any, status:any, headers:any) => {
	        console.log("ImageUpload:uploaded:", item, status, response);
	    };
	}
   	
	 onUploadSubmit(){
    const myfile = {
     img: this.img
    }
    this.authService.uploadPhoto(myfile).subscribe(data => {
      if(data.success){
        this.authService.storeUserData(data.token, data.myfile);
        this.flashMessage.show('You have uploaded the a Profile photo!', {
          cssClass: 'alert-success', 
          timeout: 5000});
      } else {
        this.flashMessage.show(data.msg, {
          cssClass: 'alert-danger', 
          timeout: 5000});
      }

    });
  }

 
}

   	/*upload() {
        let inputEl: HTMLInputElement = this.el.nativeElement.querySelector('#photo');
        let myfileCount: number = inputEl.files.length;
        let formData = new FormData();
        if (myfileCount > 0) { 
                formData.append('photo', inputEl.files.item(0));
            this.http
        //post the form data to the url defined above and map the response. Then subscribe //to initiate the post. if you don't subscribe, angular wont post.
                .post(URL, formData).map((res:Response) => res.json()).subscribe(
                 (success) => {
                         alert(success._body);
                },
                (error) => alert(error))
          }
    }*/

