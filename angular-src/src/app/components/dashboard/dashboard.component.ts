import { Component, OnInit } from '@angular/core';
import {User} from '../../user';
import {AuthService}  from '../../services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  public username: String;  
  user: User;
  constructor(private authService: AuthService) { }

  ngOnInit() {
  	/*this.authService.getProfile().subscribe(profile => {
      console.log(profile);
  		this.user = profile.user;
  	}, err => {
  		console.log(err);
  		return false;
  	}); */
  } 
}
