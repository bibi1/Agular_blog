/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { BloComponent } from './blo.component';

describe('BloComponent', () => {
  let component: BloComponent;
  let fixture: ComponentFixture<BloComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BloComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BloComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
