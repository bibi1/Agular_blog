import { Injectable } from '@angular/core';
import {Http, Headers } from '@angular/http';
import 'rxjs/add/operator/map';
import {tokenNotExpired, JwtHelper} from 'angular2-jwt';
import {BlogService} from './blog.service';


@Injectable()
export class AuthService {
	authToken: any;
	user: any;


  constructor(private http: Http) { }

  registerUser(user){
  	let headers = new Headers();
  	headers.append('Content-Type', 'application/json');
  	return this.http.post('http://localhost:3000/users/register', user, {headers: headers}) //CHANGE BECOS OF HEROKU
  	//return this.http.post('https://pure-shore-37926.herokuapp.com/users/register', user, {headers: headers})
    .map(res => res.json());
  }

  authenticateUser(user){
  	let headers = new Headers();
  	headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/users/authenticate', user, {headers: headers})
  	//return this.http.post('users/authenticate', user, {headers: headers})
  	.map(res => res.json());
  }

  getAllUsers(){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/getAllUsers', {headers: headers})
    //return this.http.post('users/updateUser', user, {headers: headers})
    .map(res => res.json());
  }

  getProfile(){
  	let headers = new Headers();
  	this.loadToken();
  	headers.append('Authorization', this.authToken);
  	headers.append('Content-Type', 'application/json');
  	return this.http.get('http://localhost:3000/users/profile', {headers: headers})
  	.map(res => res.json());
  }

  getPublicProfile(_id){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.get('http://localhost:3000/users/publicProfile/' +  _id, {headers: headers})
    .map(res => res.json());
  }

  updateUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.put('http://localhost:3000/users/updateUser', user, {headers: headers})
    //return this.http.post('users/updateUser', user, {headers: headers})
    .map(res => res.json());
  }

  deleteUser(user){
    let headers = new Headers();
    headers.append('Content-Type', 'application/json');
    return this.http.delete('http://localhost:3000/users/deleteUser', {headers: headers})
    .map(res => res.json());
  }

 uploadPhoto(myfile){
    let headers = new Headers();
    this.loadToken();
    headers.append('Authorization', this.authToken);
    headers.append('enctype', 'multipart/form-data');
    headers.append('Content-Type', 'application/json');
    return this.http.post('http://localhost:3000/upload', myfile,  {headers: headers})
    .map(res => res.json());
  }  

  storeUserData(token, user){
  	localStorage.setItem('id_token', token);
  	localStorage.setItem('user', JSON.stringify(user));
  	this.authToken = token;
  	this.user = user;
  }

  loadToken(){
  	const token = localStorage.getItem('id_token');
  	this.authToken = token;
  }

  loggedIn(){
  	return tokenNotExpired('id_token');
  }

  /*getIdUser(){
    const token = localStorage.getItem('user');
    const obj = JSON.parse(token);
    return obj._id;
  }*/

  displayForm(){
  }

  logout(){
  	this.authToken = null;
  	this.user = null;
  	localStorage.clear();
  }
}
