import { Injectable } from '@angular/core';
import {AuthService} from './auth.service';
import {Http, Headers, RequestOptions} from '@angular/http';

@Injectable()

export class BlogService {
  options;
  authToken: any;
  blog: any;
  id: any;


  constructor(
  	private authService: AuthService,
  	private http: Http
  	) { }

   // Fuction to create headers
   createAuthenticationHeaders(){
  	this.authService.loadToken();
  	this.options = new RequestOptions({
  		headers: new Headers({
  			'Content-Type': 'application/json',
  			'authorization': this.authService.authToken
  		})
  	});
  }

  newBlog(blog){
  	this. createAuthenticationHeaders;
  	return this.http.post('http://localhost:3000/blogs/newBlog', blog, this.options)
  	.map(res => res.json());
  }

   storeBlogData(token, blog){
    localStorage.setItem('id_token', token);
    localStorage.setItem('blog', JSON.stringify(blog));
    this.authToken = token;
    this.blog = blog;
  }

  loadToken(){
    const token = localStorage.getItem('id_token');
    this.authToken = token;
  }

  getAllBlogs(){
    this.createAuthenticationHeaders();
    return this.http.get('http://localhost:3000/blogs/allBlogs', this.options)
    .map(res => res.json());
  }

  getSingleBlog(id){
    this.createAuthenticationHeaders();
    return this.http.get('http://localhost:3000/blogs/singleBlog/' + id, this.options)
    .map(res => res.json());
  }

  editBlog(blog){
    this.createAuthenticationHeaders();
    return this.http.put('http://localhost:3000/blogs/updateBlog/'+blog._id, blog, this.options)
    .map(res => res.json());
  }

  deleteBlog(id){
    this.createAuthenticationHeaders();
    return this.http.delete('http://localhost:3000/blogs/deleteBlog/' + id, this.options)
    .map(res => res.json());
  }

  likeBlog(id){
    const blogData = { id: id};
    return this.http.put('http://localhost:3000/blogs/likeBlog/', blogData, this.options)
    .map(res => res.json());
  }

  dislikeBlog(id){
    const blogData = { id: id};
    return this.http.put('http://localhost:3000/blogs/dislikeBlog/', blogData, this.options)
    .map(res => res.json());
  }

   postComment(id, comment){
    this.createAuthenticationHeaders();
  // Create blogData to pass to backend
    const blogData = { 
      id: id,
      comment: comment
    };
    return this.http.post('http://localhost:3000/blogs/comment', blogData, this.options)
    .map(res => res.json());
  }

}
	