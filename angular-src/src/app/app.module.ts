import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,  ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import {DropdownModule} from 'ng2-dropdown';
//import { AngularFireAuthModule } from 'angularfire2/auth';

import { FileSelectDirective } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { HomeComponent } from './components/home/home.component';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { ProfileComponent } from './components/profile/profile.component';
import { PublicProfileComponent } from './components/public-profile/public-profile.component';


import {BlogService} from './services/blog.service';
import {ValidateService} from './services/validate.service';
import {AuthService} from './services/auth.service';
//import {UploadService} from './services/upload.service';
import {FlashMessagesModule} from 'angular2-flash-messages';
import {AuthGuard} from './guards/auth.guard';
import { BlogComponent } from './components/blog/blog.component';
import { EditBlogComponent } from './components/blog/edit-blog/edit-blog.component';
import { DeleteBlogComponent } from './components/blog/delete-blog/delete-blog.component';
import { ListUsersComponent } from './components/list-users/list-users.component';
import { UploadComponent } from './components/upload/upload.component';

const appRoutes: Routes = [
  {path:'', component: HomeComponent},
  {path:'register',component: RegisterComponent},
  {path:'login', component: LoginComponent},
  {path:'dashboard', component: DashboardComponent, canActivate:[AuthGuard]},
  {path:'profile', component: ProfileComponent},
  {path: 'user/:_id', component: PublicProfileComponent, canActivate: [AuthGuard]},
  {path:'blog', component: BlogComponent, canActivate:[AuthGuard]},
  {path: 'list-users', component: ListUsersComponent, canActivate: [AuthGuard]},
  {path: 'edit-blog/:id', component: EditBlogComponent, canActivate: [AuthGuard]},
  {path: 'delete-blog/:id', component: DeleteBlogComponent, canActivate: [AuthGuard]},

]

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    LoginComponent,
    RegisterComponent,
    HomeComponent,
    DashboardComponent,
    ProfileComponent,
    BlogComponent,
    EditBlogComponent,
    EditBlogComponent,
    DeleteBlogComponent,
    PublicProfileComponent,
    ListUsersComponent,
    UploadComponent,
    FileSelectDirective
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    FlashMessagesModule, 
    DropdownModule,
    ReactiveFormsModule
    //AngularFireAuthModule 
   
  ],
  providers: [ValidateService, AuthService, AuthGuard, BlogService],
  bootstrap: [AppComponent]
})
export class AppModule { }
