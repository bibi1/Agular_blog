export class User {
    
    constructor(
        public name : String,
        public username: String,
        public email: String,
        public password : String
    ){} 

    static CreateDefault(): User {
        return new User('', '', '', '');
    }
}
