const express = require('express');
const router = express.Router();
const path = require('path');
const bodyParser = require('body-parser');
const fs = require('fs');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');
const blogs = require('./routes/blogs')(router);


const multer = require('multer');
const storage = multer.diskStorage({
  destination: function(req, file, cb){
  	cb(null, 'uploads/');
  },
  filename: function (req, file, cb) {
  	if(!file.originalname.match(/\.(jpg|png|jpeg)$/)){
  		const err = new Error();
  		err.code = 'filetype';
  		return cb(err);
  	} else{
  		cb(null, file.originalname  + '-' + Date.now() + '.jpg');
  	}
  }
});
const upload = multer({ storage: storage, limits: { fileSize: 10000000}}).single('myfile');


//Connect to database
//mongoose.connect(config.database);
mongoose.connect(config.database, { useMongoClient: true });

// On connection
mongoose.connection.on('connected', function() {
	console.log('connected to database ' +config.database);
});

//On error
mongoose.connection.on('error', function(err) {
	console.log('Database error: ' +err);
});

const users = require('./routes/users');
//const tasks = require('./routes/tasks');

const app = express();

const port = 3000;
// connecting to HEROKU const port = process.env.PORT || 8080;

// CORS MW
app.use(cors());

// set static folder
app.use(express.static(path.join(__dirname, 'public')));

//body parser MW
app.use(bodyParser.json());
//app.use(bodyParser.urlencoded({extended: false});
//app.use(methodOverride());

//Passport MiddleWare
app.use(passport.initialize());
app.use(passport.session());

require('./config/passport')(passport);

app.use('/users', users);

app.use('/blogs', blogs)
//Index  Route
app.get('/', function(req, res){
	res.send('Invalid Endpoint');
});

app.get('*', function(req, res){
	res.sendFile(path.join(__dirname, 'public/index.html'));
});	



//View Engine
/*app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
*/


app.post('/upload', passport.authenticate('jwt', {session:false}), function(req, res){
  upload(req, res, function(err){
  	if(err){
  		if(err.code === 'LIMIT_FILE_SIZE'){
  			res.json({ success: false, message: 'File is too large. Max limit is 10MB'});
  		} else if(err.code === 'filetye'){
  			res.json({ success: false, message: 'Filetype is invaid. Must be .jpg' });
  		} else {
  			console.log(err);
  			res.json({ success: false, message: 'File was not able to uploaded' });
  		}
  	} else {
      console.log(req.file);
  		if (!req.file){
  			res.json({ success: false, message: 'No file was selected' });
  		} else{
  			res.json({  success: true, message: 'File was uploaded'});
          req.user.img.data = fs.readFileSync(req.file.path);
          req.user.img.contentType = 'image/png';
          req.user.save();
          /*req.user.save(function (err, req.user) {
            if (err) {
              res.json({ success: false, message: 'File not saved'});
    		  } else {
            res.json({ success: true, message: 'File saved'});
          }
        });*/
      }
  	} 
  });	
});

app.listen(port, function(){
	console.log('Server started on port ' + port);
});

